# Desafio VEK
## Gabriel Dias de Abreu, gabrieldiasdev@gmail.com



## This project is intended to solve exercise at docs and was proposed by VEK serviços.

## Getting Started

Run
    "flutter packages pub run build_runner build --delete-conflicting-outputs"
to generate hive database files

This project runs on Beta branch with flutter web support and Android Mobile.
Iphone support wasn't tested but is intended.

## Future Work
+ put all colors selection in a file
+ put all text in a file
+ put all validationForms in a file

## tested on
[✓] Flutter (Channel beta, v1.12.13+hotfix.6, on Linux, locale en_US.UTF-8)
    • Flutter version 1.12.13+hotfix.6 at /home/usr/.flutterSDK/flutter
    • Framework revision 18cd7a3601 (5 days ago), 2019-12-11 06:35:39 -0800
    • Engine revision 2994f7e1e6
    • Dart version 2.7.0
