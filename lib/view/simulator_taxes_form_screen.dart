import 'package:desafiovek/model/Entities/taxes_info.dart';
import 'package:desafiovek/modelview/simulation_modelview.dart';
import 'package:desafiovek/utils/custom_app_bar.dart';
import 'package:desafiovek/utils/custom_dropdown_field.dart';
import 'package:desafiovek/utils/custom_text_field.dart';
import 'package:desafiovek/utils/hint_text.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class SimulatorTaxesFormScreen extends StatelessWidget {
  GlobalKey<FormState> _formTaxes = GlobalKey<FormState>();

  Widget _form(BuildContext context) {
    SimulationModelView modelView = Provider.of<SimulationModelView>(context);
    return Form(
      key: _formTaxes,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CustomDropdownField(
            validator: (value) {
              if (value == null) return "Selecione o concorrente";
            },
            onSaved: (value) {
              modelView.taxesInfo.concorrente = value;
            },
            hint: "Concorrente *",
            listElements: modelView.getListConcorrents(),
          ),
          Text(
            "Débito",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: CustomTextField(
                  hint: 'Taxa do concorrente *',
                  textInputType: TextInputType.number,
                  onSaved: (value) => modelView
                      .taxesInfo.debito_taxa_concorrente = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    return null;
                  },
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: CustomTextField(
                  hint: 'Desconto oferecido *',
                  textInputType: TextInputType.number,
                  onSaved: (value) => modelView.taxesInfo
                      .debito_taxa_desconto_oferecido = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    if (double.parse(value) > 100.0)
                      return "desconto não pode ser maior que 100%";
                    return null;
                  },
                ),
              ),
            ],
          ),
          Text(
            "Crédito",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: CustomTextField(
                  hint: 'Taxa do concorrente *',
                  textInputType: TextInputType.number,
                  onSaved: (value) => modelView
                      .taxesInfo.credito_taxa_concorrente = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    return null;
                  },
                ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: CustomTextField(
                  hint: 'Desconto oferecido *',
                  textInputType: TextInputType.number,
                  onSaved: (value) => modelView.taxesInfo
                      .credito_taxa_desconto_oferecido = double.parse(value),
                  validator: (value) {
                    if (value == null || value == '') return "insira um valor";
                    if (double.parse(value) < 0.0)
                      return "taxas devem ser maiores que 0.0";
                    if (double.parse(value) > 100.0)
                      return "desconto não pode ser maior que 100%";
                    return null;
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottonButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: customAppBar(),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  HintText(
                      title: 'Informações de taxas',
                      description:
                          "Os campos obrigatórios estão sinalizados com *"),
                  _form(context),
                ]),
          ),
        ],
      ),
    );
  }

  _bottonButton(BuildContext context) {
    return RaisedButton(
      color: Colors.grey,
      onPressed: () => _onClickSimular(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Simular",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.navigate_next)
        ],
      ),
    );
  }

  _onClickSimular(BuildContext context) {
    SimulationModelView modelView = Provider.of<SimulationModelView>(context);

    if (_formTaxes.currentState.validate()) {
      modelView.taxesInfo = TaxesInfo("", 0.0, 0.0, 0.0, 0.0);
      _formTaxes.currentState.save();

      Navigator.pushNamed(context, '/aproveReject');
    }
  }
}
