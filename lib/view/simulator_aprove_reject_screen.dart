import 'package:desafiovek/model/Entities/simulation.dart';
import 'package:desafiovek/modelview/simulation_modelview.dart';
import 'package:desafiovek/utils/custom_app_bar.dart';
import 'package:desafiovek/utils/hint_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class SimulatorAproveRejectScreen extends StatelessWidget {
  _summaryTable(BuildContext context) {
    SimulationModelView simulationModelView =
        Provider.of<SimulationModelView>(context);
    return Table(
//                    defaultColumnWidth: FixedColumnWidth(150.0),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.grey,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: [
        TableRow(children: [
          TableCell(
            child: Text("Tipo"),
          ),
          TableCell(
            child: Text("Concorrente"),
          ),
          TableCell(
            child: Text("Proposta"),
          )
        ]),
        TableRow(children: [
          TableCell(
            child: Text("Débito"),
          ),
          TableCell(
            child: Text(
                "${simulationModelView.taxesInfo.debito_taxa_concorrente.toString()}%"),
          ),
          TableCell(
            child: Text(
              "${simulationModelView.taxesInfo.debitTaxesAfterDiscount.toStringAsFixed(2)}%",
              style: simulationModelView.areDebitTaxesUnderLimit()
                  ? TextStyle(color: Colors.green)
                  : TextStyle(color: Colors.red),
            ),
          )
        ]),
        TableRow(children: [
          TableCell(
            child: Text("Crédito"),
          ),
          TableCell(
            child: Text(
                "${simulationModelView.taxesInfo.credito_taxa_concorrente.toString()}%"),
          ),
          TableCell(
            child: Text(
              "${simulationModelView.taxesInfo.creditTaxesAfterDiscount.toStringAsFixed(2)}%",
              style: simulationModelView.areCreditTaxesUnderLimit()
                  ? TextStyle(color: Colors.green)
                  : TextStyle(color: Colors.red),
            ),
          )
        ]),
      ],
    );
  }

  _taxesUnderLimitScreen(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottonButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text(
          "Simulador",
          style: TextStyle(fontSize: 24),
        ),
      ),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  HintText(
                      title: 'Taxas da simulação permitidas',
                      description: 'Todas as taxas simuladas são permitidas.\n'
                          'Ofereça ao seu cliente'),
                  _summaryTable(context),
                ]),
          ),
        ],
      ),
    );
  }

  _taxesAboveLimitScreen(BuildContext context) {
    SimulationModelView simulationModelView =
        Provider.of<SimulationModelView>(context);
    return Scaffold(
      bottomNavigationBar: _bottonButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: customAppBar(),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  HintText(
                      title: 'Taxas da simulação não permitidas',
                      description: 'A(s) taxa(s) simulada(s) não foi(ram) '
                          'permitidas. Pois, não atinge(m) o mínimo requisitado'),
                  _summaryTable(context),
                  Padding(
                    padding: const EdgeInsets.only(top: 32.0),
                    child: Text(
                      "O valor da taxa mínima para débito é "
                      "${simulationModelView.getMinimalTaxesByActivity()['taxa_debito']}%"
                      " enquanto para crédito é "
                      "${simulationModelView.getMinimalTaxesByActivity()['taxa_credito']}% ",
                      style: TextStyle(fontSize: 10),
                    ),
                  )
                ]),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    SimulationModelView modelView = Provider.of<SimulationModelView>(context);
    if (modelView.areTaxesUnderLimit())
      return _taxesUnderLimitScreen(context);
    else
      return _taxesAboveLimitScreen(context);
  }

  _bottonButton(BuildContext context) {
    SimulationModelView simulationModelView =
        Provider.of<SimulationModelView>(context);

    if (!simulationModelView.areTaxesUnderLimit())
      return RaisedButton(
        color: Colors.grey,
        onPressed: () => Navigator.of(context).pop(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Reajustar",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      );

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        RaisedButton(
          color: Colors.grey,
          onPressed: () {
            simulationModelView.add(FinishedSimulation(
                simulationModelView.client,
                simulationModelView.taxesInfo,
                true,
                DateTime.now()));
            _acceptDialog(context);
          },
//          Navigator.push(context,
//          MaterialPageRoute(builder: (context) => ())),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Proposta aceita",
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ),
        FlatButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text("Recusar"),
        )
      ],
    );
  }

  Future _acceptDialog(BuildContext context) {
    return showDialog<String>(
        context: context,
        barrierDismissible: false,
        // dialog is dismissible with a tap on the barrier
        builder: (BuildContext context) {
          return AlertDialog(
              title: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Aceite do Cliente"),
              Text(
                "Resposta gravada no banco de dados!\n",
                style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
              ),

              // make buttons use the appropriate styles for cards
              RaisedButton(
                onPressed: () =>
                    Navigator.of(context).popUntil((route) => route.isFirst),
                color: Colors.black54,
                child: Text("Voltar para o início",
                    style: TextStyle(color: Colors.white)),
              ),
            ],
          ));
        });
  }
}
