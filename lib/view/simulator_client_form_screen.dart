import 'package:desafiovek/model/Entities/client.dart';
import 'package:desafiovek/modelview/simulation_modelview.dart';
import 'package:desafiovek/utils/custom_app_bar.dart';
import 'package:desafiovek/utils/custom_dropdown_field.dart';
import 'package:desafiovek/utils/custom_text_field.dart';
import 'package:desafiovek/utils/hint_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:provider/provider.dart';

class SimulatorClientFormScreen extends StatelessWidget {
  GlobalKey<FormState> _formClient = GlobalKey<FormState>();

  Widget _form(BuildContext context) {
    SimulationModelView modelView = Provider.of<SimulationModelView>(context);
    return Form(
      key: _formClient,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: CustomTextField(
                    hint: 'CPF *',
                    controller: MaskedTextController(
                      mask: '000.000.000-00',
                    ),
                    onSaved: (value) => modelView.client.cpf = value,
                    textInputType: TextInputType.number,
                    validator: (value) {
                      if (value.length < 14) return "CPF inválido";
                    }),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: CustomTextField(
                    hint: 'Telefone *',
                    controller: MaskedTextController(
                      mask: '(00) 0 0000-0000',
                    ),
                    textInputType: TextInputType.phone,
                    onSaved: (value) => modelView.client.telefone = value,
                    validator: (value) {
                      if (value.length < 16) return "Telefone inválido";
                    }),
              ),
            ],
          ),
          CustomTextField(
            hint: 'Email',
            onSaved: (value) => modelView.client.email = value,
            textInputType: TextInputType.emailAddress,
          ),
          CustomDropdownField(
            validator: (value) {
              if (value == null) return "Selecione o ramo de atividade";
              return null;
            },
            onSaved: (value) {
              modelView.client.ramo_atividade = value;
            },
            hint: "Ramo de atividade *",
            listElements: modelView.getListRamosAtividade(),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottonButton(context),
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      appBar: customAppBar(),
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  HintText(
                      title: 'Dados do cliente',
                      description:
                          "Os campos obrigatórios estão sinalizados com *"),
                  _form(context)
                ]),
          ),
        ],
      ),
    );
  }

  _bottonButton(BuildContext context) {
    return RaisedButton(
      color: Colors.grey,
      onPressed: () => _goNextScreen(context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Próximo",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.navigate_next)
        ],
      ),
    );
  }

  _goNextScreen(BuildContext context) {
    if (_formClient.currentState.validate()) {
      Provider.of<SimulationModelView>(context).client = Client('', '', '', '');
      _formClient.currentState.save();
      Navigator.pushNamed(context, '/taxesForm');
    }
  }
}
