import 'package:desafiovek/modelview/simulation_modelview.dart';
import 'package:desafiovek/view/csv_screen.dart';
import 'package:desafiovek/utils/custom_app_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var modelView = Provider.of<SimulationModelView>(context);
    return Scaffold(
      appBar: customAppBar(),
      body: Center(
        heightFactor: 2,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
            RaisedButton(
              color: Colors.grey,
              onPressed: () => Navigator.pushNamed(context, "/clientForm"),
              child: Text(
                "Nova Simulação",
                style: TextStyle(color: Colors.white),
              ),
            ),
            MaterialButton(
              onPressed: () {
                if (!kIsWeb) return modelView.openCsv(context);
                var csv = modelView.geraCsv();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => CsvScreen(
                              data: csv,
                            )));
              },
              shape: Border.all(color: Colors.black),
              child: Text(
                "Visualizar propostas aceitas",
                style: TextStyle(),
              ),
            ),
//            _testListSimulation(),
          ]),
        ),
      ),
    );
  }
}
