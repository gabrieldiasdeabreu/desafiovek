import 'package:hive/hive.dart';

part 'taxes_info.g.dart';

@HiveType()
class TaxesInfo extends HiveObject {
  @HiveField(0)
  String concorrente;

  @HiveField(1)
  double debito_taxa_concorrente;

  @HiveField(2)
  double credito_taxa_concorrente;

  @HiveField(3)
  double debito_taxa_desconto_oferecido;

  @HiveField(4)
  double credito_taxa_desconto_oferecido;

  TaxesInfo(
      this.concorrente,
      this.credito_taxa_concorrente,
      this.credito_taxa_desconto_oferecido,
      this.debito_taxa_concorrente,
      this.debito_taxa_desconto_oferecido);

//  @override
//  String toString() {
//    return 'TaxesInfo{concorrente: $concorrente, debito_taxa_concorrente: $debito_taxa_concorrente, credito_taxa_concorrente: $credito_taxa_concorrente, debito_taxa_oferecido: $debito_taxa_desconto_oferecido, credito_taxa_oferecido: $credito_taxa_desconto_oferecido}';
//  }

  double get creditTaxesAfterDiscount {
    return this.credito_taxa_concorrente -
        this.credito_taxa_concorrente *
            this.credito_taxa_desconto_oferecido /
            100;
  }

  double get debitTaxesAfterDiscount {
    return this.debito_taxa_concorrente -
        this.debito_taxa_concorrente *
            this.debito_taxa_desconto_oferecido /
            100;
  }
}
