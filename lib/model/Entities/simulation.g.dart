// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'simulation.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FinishedSimulationAdapter extends TypeAdapter<FinishedSimulation> {
  @override
  FinishedSimulation read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FinishedSimulation(
      fields[0] as Client,
      fields[1] as TaxesInfo,
      fields[2] as bool,
      fields[3] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, FinishedSimulation obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.client)
      ..writeByte(1)
      ..write(obj.taxesInfo)
      ..writeByte(2)
      ..write(obj.accepted)
      ..writeByte(3)
      ..write(obj.data_aceite);
  }
}
