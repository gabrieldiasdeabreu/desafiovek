import 'package:desafiovek/model/Entities/taxes_info.dart';
import 'package:hive/hive.dart';

import 'client.dart';

part 'simulation.g.dart';

@HiveType()
class FinishedSimulation extends HiveObject {
  @HiveField(0)
  Client client;

  @HiveField(1)
  TaxesInfo taxesInfo;

  @HiveField(2)
  bool accepted;

  @HiveField(3)
  DateTime data_aceite;

  FinishedSimulation(
      this.client, this.taxesInfo, this.accepted, this.data_aceite);

//  @override
//  String toString() {
//    return 'FinishedSimulation{client: $client, taxesInfo: $taxesInfo, accepted: $accepted, aceite:$data_aceite}';
//  }
}
