// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'taxes_info.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class TaxesInfoAdapter extends TypeAdapter<TaxesInfo> {
  @override
  TaxesInfo read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TaxesInfo(
      fields[0] as String,
      fields[2] as double,
      fields[4] as double,
      fields[1] as double,
      fields[3] as double,
    );
  }

  @override
  void write(BinaryWriter writer, TaxesInfo obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.concorrente)
      ..writeByte(1)
      ..write(obj.debito_taxa_concorrente)
      ..writeByte(2)
      ..write(obj.credito_taxa_concorrente)
      ..writeByte(3)
      ..write(obj.debito_taxa_desconto_oferecido)
      ..writeByte(4)
      ..write(obj.credito_taxa_desconto_oferecido);
  }
}
