import 'package:hive/hive.dart';

part 'client.g.dart';

@HiveType()
class Client extends HiveObject {
  @HiveField(0)
  String cpf;

  @HiveField(1)
  String telefone;

  @HiveField(2)
  String email;

  @HiveField(3)
  String ramo_atividade;

  Client(this.cpf, this.telefone, this.email, this.ramo_atividade);

//  @override
//  String toString() {
//    return 'Client{cpf: $cpf, telefone: $telefone, email: $email, ramo_atividade: $ramo_atividade}';
//  }
}
