import 'package:desafiovek/model/Entities/simulation.dart';
import 'package:desafiovek/model/Entities/taxes_info.dart';
import 'package:desafiovek/model/Entities/client.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

import 'package:path_provider/path_provider.dart';

initHive() async {
  if (!kIsWeb) {
    final appDocumentDir = await getApplicationDocumentsDirectory();
    Hive.init(appDocumentDir.path);
  }

  Hive.registerAdapter(ClientAdapter(), 0);
  Hive.registerAdapter(TaxesInfoAdapter(), 1);
  Hive.registerAdapter(FinishedSimulationAdapter(), 2);
//  Hive.registerAdapter(ClientAdapter(), 0);
//
  await Hive.openBox('Client');
  await Hive.openBox('TaxesInfo');
  return await Hive.openBox('Simulation');
}
