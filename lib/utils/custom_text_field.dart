import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomTextField extends StatelessWidget {
  String hint;

  TextEditingController controller;

  Function(String) onSaved;

  Function(String) validator;

  TextInputType textInputType;

  CustomTextField(
      {this.hint,
      this.controller,
      this.onSaved,
      this.validator,
      textInputType}) {
    this.textInputType = textInputType;
    if (textInputType == null) this.textInputType = TextInputType.text;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              this.hint,
              style: TextStyle(fontSize: 12),
            ),
            TextFormField(
              controller: controller,
              onSaved: (value) => this.onSaved(value),
              validator: (value) {
                if (this.validator != null) return this.validator(value);
                return null;
              },
              keyboardType: this.textInputType,
            ),
          ]),
    );
  }
}
