import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CustomDropdownField extends StatefulWidget {
  String hint;

  List<String> listElements;

  Function(String) onSaved;

  Function(String) validator;

  CustomDropdownField(
      {this.hint, this.listElements, this.onSaved, this.validator});

  @override
  _CustomDropdownFieldState createState() => _CustomDropdownFieldState();
}

class _CustomDropdownFieldState extends State<CustomDropdownField> {
  String selected;

  _dropdown() {
    return FormField<String>(
      validator: (_) { return widget.validator(this.selected);},
      onSaved: (_) => widget.onSaved(this.selected),
      builder: (
        FormFieldState<String> state,
      ) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            InputDecorator(
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(0.0),
                labelText: widget.hint,
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: Text("Selecione"),
                  value: selected,
                  onChanged: (String newValue) {
//                    state.didChange(newValue);
                    setState(() {
                      selected = newValue;
                    });
                  },
                  items: widget.listElements.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              ),
            ),
//            SizedBox(height: 5.0),
            Text(
              state.hasError ? state.errorText : '',
              style:
                  TextStyle(color: Colors.redAccent.shade700, fontSize: 12.0),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _dropdown();
  }
}
