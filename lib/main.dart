import 'package:desafiovek/model/database.dart';
import 'package:desafiovek/modelview/simulation_modelview.dart';
import 'package:desafiovek/utils/custom_app_bar.dart';
import 'package:desafiovek/view/first_screen.dart';
import 'package:desafiovek/view/simulator_aprove_reject_screen.dart';
import 'package:desafiovek/view/simulator_client_form_screen.dart';
import 'package:desafiovek/view/simulator_taxes_form_screen.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';


main() async {
//  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _openDb();
  }

  _openDb() {
    return FutureBuilder(
      future: initHive(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(create: (_) => SimulationModelView()),
            ],
            child: MaterialApp(
              title: 'Simulação',
              theme: ThemeData(
                primarySwatch: Colors.grey,
              ),
              routes: {
                '/clientForm': (context) => SimulatorClientFormScreen(),
                '/taxesForm': (context) => SimulatorTaxesFormScreen(),
                '/aproveReject': (context) => SimulatorAproveRejectScreen(),
              },
              home: snapshot.error == null ? FirstScreen() : _errorScreen(),
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  _errorScreen() {
    return Scaffold(
      appBar: customAppBar(),
      body: Center(
        child: Text('Algo deu errado :('),
      ),
    );
  }
}
