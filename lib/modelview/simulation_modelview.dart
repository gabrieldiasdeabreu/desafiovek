import 'dart:io';

import 'package:csv/csv.dart';
import 'package:desafiovek/model/Entities/client.dart';
import 'package:desafiovek/model/Entities/simulation.dart';
import 'package:desafiovek/model/Entities/taxes_info.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:hive/hive.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

class SimulationModelView extends ChangeNotifier {
  Box simulationBox;

  Client client;

  TaxesInfo taxesInfo;

  // SimulationModelView() {
  //   Hive.openBox('Simulation');
  // }

  add(FinishedSimulation newSimulation) {
    Hive.box('Simulation').add(newSimulation);
    notifyListeners();
  }

  getAll() {
    return Hive.box('Simulation').values.toList();
  }

  bool areTaxesUnderLimit() {
    ///todo: calcular com taxa de desconto
    return areDebitTaxesUnderLimit() && areCreditTaxesUnderLimit();
  }

  Map getMinimalTaxesByActivity() {
    // suppose some API consulting with Dio package
    Map taxes = {
      'atividadeA': {"taxa_debito": 10.2, "taxa_credito": 3.4},
      'atividadeB': {"taxa_debito": 3.8, "taxa_credito": 2.6},
      'atividadeC': {"taxa_debito": 7.5, "taxa_credito": 6.2},
    };

    return taxes[client.ramo_atividade];
  }

  bool areDebitTaxesUnderLimit() {
    Map taxes = getMinimalTaxesByActivity();
    return taxes["taxa_debito"] < taxesInfo.debitTaxesAfterDiscount;
  }

  bool areCreditTaxesUnderLimit() {
    Map taxes = getMinimalTaxesByActivity();
    return taxes["taxa_credito"] < taxesInfo.creditTaxesAfterDiscount;
  }

  geraCsv() {
    List<List<dynamic>> rows = List<List<dynamic>>();

    // titles of rows
    List<dynamic> row = List();

    [
      'concorrente',
      'cpf',
      'email',
      'ramo_atividade',
      'telefone',
      'debito_taxa_concorrente',
      'credito_taxa_concorrente',
      'debito_taxa_desconto_oferecido',
      'credito_taxa_desconto_oferecido',
      'debito_taxa_final_aceita',
      'credito_taxa_final_aceita',
      'data_de_aceite'
    ].forEach((v) => row.add(v));
    rows.add(row);

    Hive.box('Simulation').values.forEach((simulation) {
      if (simulation.accepted == false) return null;
      List<dynamic> row = List();
      row.add(simulation.taxesInfo.concorrente);

      row.add(simulation.client.cpf);
      row.add(simulation.client.email);
      row.add(simulation.client.ramo_atividade);
      row.add(simulation.client.telefone);

      row.add(simulation.taxesInfo.debito_taxa_concorrente);
      row.add(simulation.taxesInfo.credito_taxa_concorrente);

      row.add(simulation.taxesInfo.debito_taxa_desconto_oferecido);
      row.add(simulation.taxesInfo.credito_taxa_desconto_oferecido);

      row.add(simulation.taxesInfo.debitTaxesAfterDiscount);
      row.add(simulation.taxesInfo.creditTaxesAfterDiscount);

      row.add(simulation.data_aceite);
      rows.add(row);
    });

    return rows;
  }

  openCsv(BuildContext context) async {
    if (!kIsWeb) {
      final permissionValidator = EasyPermissionValidator(
        context: context,
        appName: 'Permissão para escrever o CSV',
      );

      var result = await permissionValidator.storage();

      if (result) {
        String dir = (await getExternalStorageDirectory()).absolute.path +
            "/saidaDesafioVEK.csv";
        var file = "$dir";
        File f = new File(file);

        String csv = const ListToCsvConverter().convert(geraCsv());
        await f.writeAsString(csv);

        await OpenFile.open(file); // csv type isnt supported, type: "text/csv"

      }
    } else throw Exception("WEB client can't create files and open");
  }

  getListConcorrents() {
    return ['concorrenteA', 'concorrenteB', 'concorrenteC', 'concorrenteD'];
  }

  getListRamosAtividade() {
    return ['A', 'B', 'C'].map((v) => "atividade" + v).toList();
  }
}
